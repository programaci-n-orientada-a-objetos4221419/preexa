
package modelo;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author Estrella
 */
public class CuentaBancaria {
    private static int contadorCuentas = 0;

    private int numeroCuenta;
    private String nombreBanco;
    private String nombreCliente;
    private String fechaApertura;
    private float porcentajeRendimiento;
    private float saldo;

    public CuentaBancaria(String nombreBanco, String nombreCliente, float porcentajeRendimiento) {
        this.numeroCuenta = ++contadorCuentas;
        this.nombreBanco = nombreBanco;
        this.nombreCliente = nombreCliente;
        this.fechaApertura = obtenerFechaActual();
        this.porcentajeRendimiento = porcentajeRendimiento;
        this.saldo = 0.0f;
    }

    public CuentaBancaria(String numeroCuenta, String nombre, String fechaNacimientoStr, String sexo, String domicilio, String nombreBanco, String fechaAperturaStr, float porcentajeRendimiento) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
     public void depositar(float cantidad) {
        saldo += cantidad;
    }

    public boolean retirar(float cantidad) {
        if (cantidad <= saldo) {
            saldo -= cantidad;
            return true;
        }
        return false;
    }
    public float calcularRendimientos() {
        float rendimientoDiario = (porcentajeRendimiento / 100) * saldo / 365;
        return rendimientoDiario;
    }
    private String obtenerFechaActual() {
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaActual = new Date();
        return formatoFecha.format(fechaActual);
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }
     public String getFechaApertura() {
        return fechaApertura;
    }

    public float getPorcentajeRendimiento() {
        return porcentajeRendimiento;
    }

    public float getSaldo() {
        return saldo;
    }

    public boolean guardarEnBaseDeDatos() {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
