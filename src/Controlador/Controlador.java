package Controlador;

import Vista.dlgCuenta;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import modelo.CuentaBancaria;

public class Controlador implements ActionListener {
    private CuentaBancaria cuenta;
    private dlgCuenta vista;
    private ButtonGroup grupoSexo;
    private JCheckBox checkBox;

    public Controlador(CuentaBancaria cuenta, dlgCuenta vista) {
        this.cuenta = cuenta;
        this.vista = vista;
        this.vista.btnNuevo.addActionListener(this);
        this.vista.btnGuardar.addActionListener(this);
        this.vista.btnMostrar.addActionListener(this);
        this.vista.btnHacerDeposito.addActionListener(this);
        this.vista.btnHacerRetiro.addActionListener(this);
        this.grupoSexo = new ButtonGroup();
        this.grupoSexo.add(this.vista.txtSexoMasculino);
        this.grupoSexo.add(this.vista.txtSexoFemenino);
        this.checkBox = new JCheckBox();
        this.checkBox.setVisible(false);
        this.vista.txtSexoMasculino.addActionListener(this);
        this.vista.txtSexoFemenino.addActionListener(this);
    }
    public void iniciarVista() {
        vista.setTitle("Cuenta de Ahorro");
        vista.setMinimumSize(new Dimension(900, 900));
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        vista.pack();
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (vista.btnNuevo == e.getSource()) {
            nuevaCuenta();
        } else if (vista.btnGuardar == e.getSource()) {
            guardarCuenta();
        } else if (vista.btnMostrar == e.getSource()) {
            mostrarCuenta();
        } else if (vista.btnHacerDeposito == e.getSource()) {
            hacerDeposito();
        } else if (vista.btnHacerRetiro == e.getSource()) {
            hacerRetiro();
        } else if (vista.txtSexoMasculino == e.getSource()) {
            checkBox.setSelected(false);
        } else if (vista.txtSexoFemenino == e.getSource()) {
            checkBox.setSelected(true);
        }
    }

    private void nuevaCuenta() {
        String numeroCuenta = JOptionPane.showInputDialog("Ingrese el número de cuenta:");
        if (numeroCuenta == null || numeroCuenta.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe ingresar un número de cuenta válido");
            return;
        }
        String nombre = JOptionPane.showInputDialog("Ingrese el nombre del titular de la cuenta:");
        if (nombre == null || nombre.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe ingresar un nombre válido");
            return;
        }

        String fechaNacimientoStr = JOptionPane.showInputDialog("Ingrese la fecha de nacimiento (dd/MM/yyyy):");
        if (fechaNacimientoStr == null || fechaNacimientoStr.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe ingresar una fecha de nacimiento válida");
            return;
        }
        String sexo = "";
        if (vista.txtSexoMasculino.isSelected()) {
            sexo = "Masculino";
        } else if (vista.txtSexoFemenino.isSelected()) {
            sexo = "Femenino";
        }
        if (sexo.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe seleccionar el sexo");
            return;
        }
        String domicilio = JOptionPane.showInputDialog("Ingrese el domicilio:");
        if (domicilio == null || domicilio.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe ingresar un domicilio válido");
            return;
        }

        String nombreBanco = JOptionPane.showInputDialog("Ingrese el nombre del banco:");
        if (nombreBanco == null || nombreBanco.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe ingresar un nombre de banco válido");
            return;
        }
        String fechaAperturaStr = JOptionPane.showInputDialog("Ingrese la fecha de apertura (dd/MM/yyyy):");
        if (fechaAperturaStr == null || fechaAperturaStr.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe ingresar una fecha de apertura válida");
            return;
        }

        float porcentajeRendimiento;
        try {
            porcentajeRendimiento = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el porcentaje de rendimiento:"));
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Debe ingresar un porcentaje de rendimiento válido");
            return;
        }
        cuenta = new CuentaBancaria(numeroCuenta, nombre, fechaNacimientoStr, sexo, domicilio, nombreBanco, fechaAperturaStr, porcentajeRendimiento);
        this.vista.txtNumeroCuenta.setText(numeroCuenta);
        this.vista.txtNombre.setText(nombre);
        this.vista.txtFechaNacimiento.setText(fechaNacimientoStr);
        this.vista.txtDomicilio.setText(domicilio);
        this.vista.txtNombreBanco.setText(nombreBanco);
        this.vista.txtFechaAPertura.setText(fechaAperturaStr);
        this.vista.txtPorcentajeRendimiento.setText(String.valueOf(porcentajeRendimiento));
        this.vista.txtSaldo.setText(String.valueOf(cuenta.getSaldo()));
        checkBox.setSelected(sexo.equals("Femenino"));
    }
    private void guardarCuenta() {
        if (cuenta == null) {
            JOptionPane.showMessageDialog(null, "No hay cuenta para guardar. Crea una nueva cuenta primero.");
            return;
        }
        boolean guardado = cuenta.guardarEnBaseDeDatos();
        if (guardado) {
            JOptionPane.showMessageDialog(null, "Cuenta guardada exitosamente.");
        } else {
            JOptionPane.showMessageDialog(null, "Error al guardar la cuenta en la base de datos.");
        }
    }

    private void mostrarCuenta() {
        if (cuenta == null) {
            JOptionPane.showMessageDialog(null, "No hay cuenta para mostrar. Crea una nueva cuenta primero.");
            return;
        }
        JOptionPane.showMessageDialog(null, "Saldo: $" + cuenta.getSaldo());
    }
    private void hacerDeposito() {
        if (cuenta == null) {
            JOptionPane.showMessageDialog(null, "No hay cuenta para hacer depósito. Crea una nueva cuenta primero.");
            return;
        }
        float cantidad;
        try {
            cantidad = Float.parseFloat(this.vista.txtcantidad.getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Inserte una cantidad válida");
            return;
        }

        cuenta.depositar(cantidad);
        this.vista.txtSaldo.setText(String.valueOf(cuenta.getSaldo()));
        JOptionPane.showMessageDialog(null, "Se ha realizado un depósito de $" + cantidad);
    }

    private void hacerRetiro() {
        if (cuenta == null) {
            JOptionPane.showMessageDialog(null, "No hay cuenta para hacer retiro. Crea una nueva cuenta primero.");
            return;
        }
        float cantidad;
        try {
            cantidad = Float.parseFloat(this.vista.txtcantidad.getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Inserte una cantidad válida");
            return;
        }

        if (cuenta.getSaldo() >= cantidad) {
            cuenta.retirar(cantidad);
            this.vista.txtSaldo.setText(String.valueOf(cuenta.getSaldo()));
        } else {
            int opcion = JOptionPane.showConfirmDialog(null, "Saldo insuficiente para realizar el retiro. ¿Desea volver a intentar?", "Advertencia", JOptionPane.YES_NO_OPTION);
            if (opcion == JOptionPane.YES_OPTION) {
                this.vista.txtcantidad.setText("");
            }
        }
    }
}
